import Card.Card;
import Game.Game;
import org.junit.*;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class GameTest {

    private static Game testGame;

    @BeforeClass
    public static void setUpClass() throws Exception {
        testGame = new Game();
    }

    @Before
    public void setUp() {
        testGame.addCard(new Card("Spade", "2", 1));
        testGame.addCard(new Card("Club", "3", 3));
        testGame.addCard(new Card("Diamond", "2", 4));
        testGame.addCard(new Card("Heart", "Jack", 10));
        testGame.addCard(new Card("Heart", "Jack", 10));
    }

    @After
    public void tearDown() {
        testGame.clearCards();
    }

    @Test
    public void drawTest() {
        testGame.draw(); // draw 1st card
        testGame.draw(); // draw 2nd card
        testGame.draw(); // draw 3rd card
        testGame.draw(); // draw 4th card
        testGame.draw(); // draw the last one
        testGame.draw(); // no more cards -> reminder!
    }

    @Test
    public void mixTest() {
        List<Card> secondCardList = new LinkedList<Card>();
        secondCardList.addAll(testGame.getCards());
        Assert.assertTrue(testGame.getCards().equals(secondCardList)); // two same card lists
        testGame.mix(); // testGame.cards are mixing
        Assert.assertFalse(testGame.getCards().equals(secondCardList)); // cards and secondCardList should be different
        Collections.shuffle(secondCardList); // shuffling secondCardList
        Assert.assertFalse(testGame.getCards().equals(secondCardList)); // cards and secondCardList should still be different
    }

    @Test
    public void compareCardTest() {
        int[][] numbers = {
                {-1, 0, 1},
                {1, 2, 1},
                {1, 3, 0},
                {0, 4, 3}};
        String[] comments = {
                "It should to return -1.",
                "It should to return 1.",
                "It should to return 1.",
                "It should to return 0."};
        for (int i = 0; i < 4; i++) {
            Assert.assertEquals(
                    comments[i],
                    numbers[i][0],
                    testGame.getCardWithIndex(numbers[i][1]).compareTo(testGame.getCardWithIndex(numbers[i][2])));
        }
    }

}
