package Menu;

public class Menu {

    public static void welcomeMenu() {
        System.out.println("###################################");
        System.out.println("#                                 #");
        System.out.println("#      THE SIMPLY CARD GAME       #");
        System.out.println("#                                 #");
        System.out.println("#      created by T. Miller       #");
        System.out.println("#                                 #");
        System.out.println("###################################");
    }

    public static void mainMenu() {
        System.out.println();
        System.out.println("[1] START NEW GAME");
        System.out.println("[2] EXIT GAME");
        System.out.println();
        System.out.println("Insert your select: ");
    }

    public static void howManyDecks() {
        System.out.println();
        System.out.println("How many decks do you want to play? ");
    }

    public static void gameMenu() {
        System.out.println();
        System.out.println("[1] MIX CARDS");
        System.out.println("[2] SORT CARDS");
        System.out.println("[3] DRAW ONE CARD");
        System.out.println("[4] EXIT TO THE MENU");
        System.out.println();
        System.out.println("Insert your select: ");
    }

}
