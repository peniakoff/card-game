package Game;

import Card.Card;
import Menu.Menu;

import java.util.*;

public class Game {

    private List<Card> cards = new LinkedList<>();
    private String[] suitArray = {"Spade", "Club", "Diamond", "Heart"};
    private String[] rankArray = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};

    public Game() {
    }

    public void newGame() {
        Scanner scanner = new Scanner(System.in);
        int deckNumber = 0;
        do {
            Menu.howManyDecks();
            deckNumber = scanner.nextInt();
        } while (deckNumber < 1);
        cards = cardList(deckNumber); // CARDS GENERATING
        mix(); // DECK'S AUTO-MIXING
        int select = 0;
        do {
            Menu.gameMenu();
            select = scanner.nextInt();
            switch (select) {
                case 1: // MIX CARDS
                    mix();
                    break;
                case 2: // SORT CARDS
                    sort();
                    break;
                case 3: // DRAW ONE CARD
                    draw();
                    break;
                case 4: // EXIT TO THE MENU
                    break;
                default:
                    System.out.println("Your select is invalid!");
                    break;
            }
        } while (select != 4);
    }

    private List<Card> cardList(int numberOfDecks) {
        List<Card> cards = new LinkedList<>();
        for (int j = 1; j <= numberOfDecks; j++) {
            int value = 1;
            for (String suit : suitArray) {
                for (String rank : rankArray) {
                    cards.add(new Card(rank, suit, value));
                    value ++;
                }
            }
        }
        return cards;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public void addCard(Card card) {
        this.cards.add(card);
    }

    public void clearCards() {
        this.cards.clear();
    }

    public Card getCardWithIndex(int index) {
        return this.cards.get(index);
    }

    // CARDS MIXING
    public void mix() {
        if (cards.size() > 1) {
            Collections.shuffle(cards);
            System.out.println("Your cards were mixed!");
        } else {
            System.out.println("You don't have enough cards to shuffle them!");
        }

    }

    // CARDS SORTING
    public void sort() {
        if (cards.size() > 1) {
            Collections.sort(cards);
            System.out.println("Your cards were sorted!");
        } else {
            System.out.println("You don't have enough cards to sort them!");
        }
    }

    // ONE CARD DRAWING
    public void draw() {
        if (cards.size() > 0) {
            Optional<Card> card = cards.stream().findFirst();
            System.out.println("Your drawned the card: " + card.get().toString());
            cards.remove(0);
        } else {
            System.out.println("You have no more cards! Exit to the menu and start new game.");
        }
    }

}
