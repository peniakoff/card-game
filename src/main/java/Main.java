import Card.Card;
import Game.Game;
import Menu.Menu;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        int select = 0;
        Scanner scanner = new Scanner(System.in);
        Menu.welcomeMenu();
        try {
            do {
                Menu.mainMenu();
                select = scanner.nextInt();
                switch (select) {
                    case 1:
                        Game game = new Game();
                        game.newGame();
                        break;
                    case 2:
                        System.out.println("Goodbye!");
                        break;
                    default:
                        System.out.println("Your select is invalid!");
                        break;
                }
            } while (select != 2);
        } catch (InputMismatchException e) {
            e.getStackTrace();
        }
    }

}
